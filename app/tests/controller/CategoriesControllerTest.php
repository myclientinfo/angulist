<?php 

class CategoriesControllerTest extends \TestCase
{
	public function testIndex()
	{
		$this->call('GET', 'category');
        $this->assertResponseOk();
    }

    public function testShow()
    {
        $this->call('GET', 'category/1');
        $this->assertResponseOk();
    }

    public function testCreate()
    {
        $this->call('GET', 'category/create');
        $this->assertResponseOk();
    }

    public function testEdit()
    {
        $this->call('GET', 'category/1/edit');
        $this->assertResponseOk();
    }
}
