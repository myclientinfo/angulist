<?php 

class LanguagesControllerTest extends \TestCase
{
	public function testIndex()
	{
		$this->call('GET', 'language');
        $this->assertResponseOk();
    }

    public function testShow()
    {
        $this->call('GET', 'language/1');
        $this->assertResponseOk();
    }

    public function testCreate()
    {
        $this->call('GET', 'language/create');
        $this->assertResponseOk();
    }

    public function testEdit()
    {
        $this->call('GET', 'language/1/edit');
        $this->assertResponseOk();
    }
}
