<?php 

class PackagesControllerTest extends \TestCase
{
	public function testIndex()
	{
		$this->call('GET', 'package');
        $this->assertResponseOk();
    }

    public function testShow()
    {
        $this->call('GET', 'package/1');
        $this->assertResponseOk();
    }

    public function testCreate()
    {
        $this->call('GET', 'package/create');
        $this->assertResponseOk();
    }

    public function testEdit()
    {
        $this->call('GET', 'package/1/edit');
        $this->assertResponseOk();
    }
}
