<?php namespace Frontend;

class PackageController extends \BaseController
{
	protected $package;

	public function __construct(PackageRepositoryInterface $package)
	{
		$this->package = $package;
	}

	public function index()
	{
    	$packages = $this->package->all();
			return $packages;
	}

	public function create()
	{
        $this->layout->content = \View::make('package.create');
	}

	public function store()
	{
        $this->package->store(\Input::only('name','homepage','problem','opinion'));
        return \Redirect::route('package.index');
	}

	public function show($id)
	{
        $package = $this->package->find($id);
				return $package;
	}

	public function edit($id)
	{
        $package = $this->package->find($id);
        $this->layout->content = \View::make('package.edit')->with('package', $package);
	}

	public function update($id)
	{
        $this->package->find($id)->update(\Input::only('name','homepage','problem','opinion'));
        return \Redirect::route('package.show', $id);
	}

	public function destroy($id)
	{
        $this->package->destroy($id);
        return \Redirect::route('package.index');
	}

}
