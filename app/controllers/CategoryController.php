<?php namespace Frontend;

class CategoryController extends \BaseController
{
	protected $category;

	public function __construct(CategoryRepositoryInterface $category)
	{
		$this->category = $category;
	}

	public function index()
	{
    	$categories = $this->category->all();
        $this->layout->content = \View::make('category.index', compact('categories'));
	}

	public function create()
	{
        $this->layout->content = \View::make('category.create');
	}

	public function store()
	{
        $this->category->store(\Input::only('name'));
        return \Redirect::route('category.index');
	}

	public function show($id)
	{
        $category = $this->category->find($id);
        $this->layout->content = \View::make('category.show')->with('category', $category);
	}

	public function edit($id)
	{
        $category = $this->category->find($id);
        $this->layout->content = \View::make('category.edit')->with('category', $category);
	}

	public function update($id)
	{
        $this->category->find($id)->update(\Input::only('name'));
        return \Redirect::route('category.show', $id);
	}

	public function destroy($id)
	{
        $this->category->destroy($id);
        return \Redirect::route('category.index');
	}

}
