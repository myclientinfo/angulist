<?php namespace Frontend;

class LanguageController extends \BaseController
{
	protected $language;

	public function __construct(LanguageRepositoryInterface $language)
	{
		$this->language = $language;
	}

	public function index()
	{
    	$languages = $this->language->all();
        $this->layout->content = \View::make('language.index', compact('languages'));
	}

	public function create()
	{
        $this->layout->content = \View::make('language.create');
	}

	public function store()
	{
        $this->language->store(\Input::only('name'));
        return \Redirect::route('language.index');
	}

	public function show($id)
	{
        $language = $this->language->find($id);
        $this->layout->content = \View::make('language.show')->with('language', $language);
	}

	public function edit($id)
	{
        $language = $this->language->find($id);
        $this->layout->content = \View::make('language.edit')->with('language', $language);
	}

	public function update($id)
	{
        $this->language->find($id)->update(\Input::only('name'));
        return \Redirect::route('language.show', $id);
	}

	public function destroy($id)
	{
        $this->language->destroy($id);
        return \Redirect::route('language.index');
	}

}
