<?php namespace Frontend;

class Package extends \Eloquent
{
  protected $table = 'packages';
  protected $fillable = array('name','homepage','problem','opinion');
  protected $hidden = ['created_at', 'updated_at'];

  public function language()
  {
    return $this->belongsTo('Frontend\Language');
  }

  public function category()
  {
    return $this->belongsTo('Frontend\Category');
  }

  public function getCategoryAttribute($value){
    return $value->name;
  }


}
