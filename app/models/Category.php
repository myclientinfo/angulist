<?php namespace Frontend;

class Category extends \Eloquent
{
    protected $table = 'categories';
	protected $fillable = array('name');
  protected $hidden = ['created_at', 'updated_at'];
	public function packages()
	{
		return $this->hasMany('Frontend\Package');
	}


}
