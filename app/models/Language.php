<?php namespace Frontend;

class Language extends \Eloquent
{
  protected $table = 'languages';
  protected $fillable = array('name');
  
  public function packages()
  {
    return $this->hasMany('Frontend\Package');
  }


}
