<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('layouts.angular');
});

App::bind('Frontend\PackageRepositoryInterface','Frontend\EloquentPackageRepository');
Route::resource('package', 'Frontend\PackageController');

App::bind('Frontend\LanguageRepositoryInterface','Frontend\EloquentLanguageRepository');
Route::resource('language', 'Frontend\LanguageController');

App::bind('Frontend\CategoryRepositoryInterface','Frontend\EloquentCategoryRepository');
Route::resource('category', 'Frontend\CategoryController');
