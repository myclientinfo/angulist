<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePackagesTable extends Migration
{

	public function up()
	{
		Schema::create('packages', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('homepage');
			$table->text('problem');
			$table->text('opinion');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
			$table->integer('language_id')->unsigned();
			$table->integer('category_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::dropIfExists('packages');
	}

}
