<?php

class PackagesTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$package = array(
				'name' => $faker->company,
				'homepage' => $faker->url,
				'problem' => $faker->text,
				'opinion' => $faker->text,
			);
			\Frontend\Package::create($package);
		}
	}

}
