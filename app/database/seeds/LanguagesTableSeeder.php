<?php 

class LanguagesTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$language = array(
				'name' => $faker->name,
			);
			\Frontend\Language::create($language);
		}
	}

}
