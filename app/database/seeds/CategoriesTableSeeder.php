<?php 

class CategoriesTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$category = array(
				'name' => $faker->name,
			);
			\Frontend\Category::create($category);
		}
	}

}
