@section('content')
<div class="row">
    <h1>All Languages</h1>
    <a class="btn btn-success" href="{{ url('language/create') }}">New</a>
</div>
<div class="row">
    {{ $languages->links() }}
</div>
<div class="row">
    <table class="table">
        <thead>
        <th>Name</th>
        </thead>
        <tbody>
        @foreach($languages as $language)
        <tr>
            
            <td>
                <a href="{{ url('language/'.$language->id) }}">{{ $language->name }}</a>
            </td>
            
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@stop
