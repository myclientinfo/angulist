@section('content')
<div class="row">
    <h2>Edit language</h2>
</div>
<div class="row">
    {{ Form::model($language, array('route' => array('language.update', $language->id), 'method' => 'PUT')) }}

    
    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>
    

    {{ Form::submit('Edit Language', array('class' => 'btn btn-success')) }}

    {{Form::close()}}
</div>
@stop

