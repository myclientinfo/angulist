@section('content')
<div class="row">
    <h1>Viewing language</h1>
    <a class="btn btn-primary" href="{{ url('language/'.$language->id .'/edit') }}">Edit</a>
    {{ Form::open(array('url' => 'language/' . $language->id, 'method' => 'DELETE')) }}
    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
    {{ Form::close() }}
</div>
<div class="row">
    <table class="table">
        
        <tr>
            <td>Name:</td>
            <td>{{ $language->name }}</td>
        </tr>
        
    </table>
</div>
@stop
