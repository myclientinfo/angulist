@section('content')
<div class="row">
    <h1>All Categories</h1>
    <a class="btn btn-success" href="{{ url('category/create') }}">New</a>
</div>
<div class="row">
    {{ $categories->links() }}
</div>
<div class="row">
    <table class="table">
        <thead>
        <th>Name</th>
        </thead>
        <tbody>
        @foreach($categories as $category)
        <tr>
            
            <td>
                <a href="{{ url('category/'.$category->id) }}">{{ $category->name }}</a>
            </td>
            
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@stop
