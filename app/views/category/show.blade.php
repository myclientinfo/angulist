@section('content')
<div class="row">
    <h1>Viewing category</h1>
    <a class="btn btn-primary" href="{{ url('category/'.$category->id .'/edit') }}">Edit</a>
    {{ Form::open(array('url' => 'category/' . $category->id, 'method' => 'DELETE')) }}
    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
    {{ Form::close() }}
</div>
<div class="row">
    <table class="table">
        
        <tr>
            <td>Name:</td>
            <td>{{ $category->name }}</td>
        </tr>
        
    </table>
</div>
@stop
