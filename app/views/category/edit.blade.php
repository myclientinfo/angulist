@section('content')
<div class="row">
    <h2>Edit category</h2>
</div>
<div class="row">
    {{ Form::model($category, array('route' => array('category.update', $category->id), 'method' => 'PUT')) }}

    
    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>
    

    {{ Form::submit('Edit Category', array('class' => 'btn btn-success')) }}

    {{Form::close()}}
</div>
@stop

