<!doctype html>
<html lang="en" ng-app="frontendApp">
<head>
  <meta charset="utf-8">
  <title>Google Phone Gallery</title>
  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <script src="/bower_components/angular/angular.js"></script>
  <script src="/bower_components/angular-route/angular-route.js"></script>
  <script src="js/app.js"></script>
  <script src="js/controllers.js"></script>
</head>
<body>

<div class="container">

  <nav class="navbar navbar-default" role="navigation">
  <ul class="nav navbar-nav">
    <li><a href="/home">Home</a></li>
    <li class="active"><a href="/home">Home</a></li>
    <li><a href="/home">Home</a></li>
  </ul>

  </nav>

  <div ng-view></div>

</div>

</body>
</html>
