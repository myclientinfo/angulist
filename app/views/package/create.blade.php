@section('content')
<div class="row">
    <h2>New Package</h2>
</div>
<div class="row">
    {{ Form::open(array('url' => 'package')) }}

    
    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('homepage', 'Homepage') }}
        {{ Form::text('homepage', Input::old('homepage'), array('class' => 'form-control')) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('problem', 'Problem') }}
        {{ Form::text('problem', Input::old('problem'), array('class' => 'form-control')) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('opinion', 'Opinion') }}
        {{ Form::text('opinion', Input::old('opinion'), array('class' => 'form-control')) }}
    </div>
    

    {{ Form::submit('Add Package', array('class' => 'btn btn-success')) }}

    {{ Form::close() }}
</div>
@stop