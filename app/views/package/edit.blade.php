@section('content')
<div class="row">
    <h2>Edit package</h2>
</div>
<div class="row">
    {{ Form::model($package, array('route' => array('package.update', $package->id), 'method' => 'PUT')) }}

    
    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('homepage', 'Homepage') }}
        {{ Form::text('homepage', null, array('class' => 'form-control')) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('problem', 'Problem') }}
        {{ Form::text('problem', null, array('class' => 'form-control')) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('opinion', 'Opinion') }}
        {{ Form::text('opinion', null, array('class' => 'form-control')) }}
    </div>
    

    {{ Form::submit('Edit Package', array('class' => 'btn btn-success')) }}

    {{Form::close()}}
</div>
@stop

