@section('content')
<div class="row">
    <h1>All Packages</h1>
    <a class="btn btn-success" href="{{ url('package/create') }}">New</a>
</div>
<div class="row">
    {{ $packages->links() }}
</div>
<div class="row">
    <table class="table">
        <thead>
        <th>Name</th><th>Homepage</th><th>Problem</th><th>Opinion</th>
        </thead>
        <tbody>
        @foreach($packages as $package)
        <tr>
            
            <td>
                <a href="{{ url('package/'.$package->id) }}">{{ $package->name }}</a>
            </td>
            
            <td>
                <a href="{{ url('package/'.$package->id) }}">{{ $package->homepage }}</a>
            </td>
            
            <td>
                <a href="{{ url('package/'.$package->id) }}">{{ $package->problem }}</a>
            </td>
            
            <td>
                <a href="{{ url('package/'.$package->id) }}">{{ $package->opinion }}</a>
            </td>
            
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@stop
