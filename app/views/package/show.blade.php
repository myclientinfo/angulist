@section('content')
<div class="row">
    <h1>Viewing package</h1>
    <a class="btn btn-primary" href="{{ url('package/'.$package->id .'/edit') }}">Edit</a>
    {{ Form::open(array('url' => 'package/' . $package->id, 'method' => 'DELETE')) }}
    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
    {{ Form::close() }}
</div>
<div class="row">
    <table class="table">
        
        <tr>
            <td>Name:</td>
            <td>{{ $package->name }}</td>
        </tr>
        
        <tr>
            <td>Homepage:</td>
            <td>{{ $package->homepage }}</td>
        </tr>
        
        <tr>
            <td>Problem:</td>
            <td>{{ $package->problem }}</td>
        </tr>
        
        <tr>
            <td>Opinion:</td>
            <td>{{ $package->opinion }}</td>
        </tr>
        
    </table>
</div>
@stop
