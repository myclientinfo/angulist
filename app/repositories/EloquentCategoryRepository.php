<?php namespace Frontend;

class EloquentCategoryRepository implements CategoryRepositoryInterface
{
	private $category;

	public function __construct(Category $category)
	{
		$this->category = $category;
	}

	public function all()
	{
		return $this->category->paginate(15);
	}

	public function find($id)
	{
		return $this->category->find($id);
	}

	public function store($input)
	{
        $category = new Category;
        
        $category->name = $input['name'];
        
        $category->save();
	}

	public function update($input)
	{
	    $this->category->update($input);
	}

	public function destroy($id)
	{
		$this->category->find($id)->delete();
	}

}
