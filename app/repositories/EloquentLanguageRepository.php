<?php namespace Frontend;

class EloquentLanguageRepository implements LanguageRepositoryInterface
{
	private $language;

	public function __construct(Language $language)
	{
		$this->language = $language;
	}

	public function all()
	{
		return $this->language->paginate(15);
	}

	public function find($id)
	{
		return $this->language->find($id);
	}

	public function store($input)
	{
        $language = new Language;
        
        $language->name = $input['name'];
        
        $language->save();
	}

	public function update($input)
	{
	    $this->language->update($input);
	}

	public function destroy($id)
	{
		$this->language->find($id)->delete();
	}

}
