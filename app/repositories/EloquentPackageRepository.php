<?php namespace Frontend;

class EloquentPackageRepository implements PackageRepositoryInterface
{
	private $package;

	public function __construct(Package $package)
	{
		$this->package = $package;
	}

	public function all()
	{
		$packages = $this->package->with('Category', 'Language')->get();
		return $packages;
	}

	public function find($id)
	{
		$package = $this->package->with('Language', 'Category')->find($id);
		return $package;
	}

	public function store($input)
	{
        $package = new Package;

        $package->name = $input['name'];

        $package->homepage = $input['homepage'];

        $package->problem = $input['problem'];

        $package->opinion = $input['opinion'];

        $package->save();
	}

	public function update($input)
	{
	    $this->package->update($input);
	}

	public function destroy($id)
	{
		$this->package->find($id)->delete();
	}

}
