'use strict';

/* App Module */

var frontendApp = angular.module('frontendApp', [
  'ngRoute',
  'frontendControllers'
]);

frontendApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/packages', {
        templateUrl: 'partials/list.html',
        controller: 'FrontendListCtrl'
      }).
      when('/packages/:packageId', {
        templateUrl: 'partials/show.html',
        controller: 'FrontendDetailCtrl'
      }).
      otherwise({
        redirectTo: '/packages'
      });
  }]);
