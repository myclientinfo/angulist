'use strict';

/* Controllers */

var frontendControllers = angular.module('frontendControllers', []);

frontendControllers.controller('FrontendListCtrl', ['$scope', '$http',
  function($scope, $http) {
    $http.get('package/').success(function(data) {
      $scope.packages = data;
    });


    $scope.orderProp = 'name';
  }]);

frontendControllers.controller('FrontendDetailCtrl', ['$scope', '$routeParams', '$http',
  function($scope, $routeParams, $http) {
    $http.get('package/' + $routeParams.packageId).success(function(data) {
      $scope.package = data;
    });


  }]);



/*



$scope.packages = [
  {
    'name': 'jQuery',
    'category': 'Library',
    'language': 'Javascript',
    'homepage': 'http://jquery.com',
    'tagline': 'It\'s a library thing',
    'description': 'This is a thing'
  }, {
    'name': 'AngularJS',
    'category': 'Framework',
    'language': 'Javascript',
    'homepage': 'http://www.angularjs.com',
    'tagline': 'A library for building stuff',
    'description': ''
  }, {
    'name': 'EmberJS',
    'category': 'Framework',
    'language': 'Javascript',
    'homepage': 'http://www.emberjs.io',
    'tagline': 'A framework for creating ambitious web applications',
    'discussion': '',
    'opinion': 'Ember is kinda hard, but has a really nice structure'
  }, {
    'name': 'NodeJS',
    'category': 'Back-end',
    'language': 'Javascript',
    'homepage': 'http://www.nodejs.org',
    'tagline': '',
    'description': '',
    'opinion': 'Node is an excellent option for high-speed, high availability backends'
  }, {
    'name': 'Gulp',
    'category': 'Tool',
    'language': 'Node',
    'homepage': 'http://www.gulp.org',
    'tagline': 'Gulp for gulping things',
    'description': 'Some stuff that gulp does',
    'opinion': 'Seems to have replaced grunt entirely'
  }
];
*/
